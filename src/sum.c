#include "sum.h"
#include "stdint.h"

int64_t integer_sum(int a, int b)
{
    int64_t tempA = a;
    int64_t tempB = b;

    return tempA + tempB;
}
