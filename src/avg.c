#include "avg.h"
#include <stdint.h>

int64_t integer_avg(int a, int b)
{
    int64_t tmpA = a;
    int64_t tmpB = b;

    return (tmpA+tmpB)/2;
}