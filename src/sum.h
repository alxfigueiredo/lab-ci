#ifndef __SUM_H
#define __SUM_H

#include "stdint.h"

/** @return The sum of two integers @ref a and @ref b. */
int64_t integer_sum(int a, int b);

#endif //__SUM_H