#ifndef __AVG_H
#define __AVG_H

#include "stdint.h"

/** @return The average of two integers @ref a and @ref b. */
int64_t integer_avg(int a, int b);

#endif //__AVG_H