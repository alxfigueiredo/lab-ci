#include "avg.h"
#include "unity.h"

#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>

void setUp(void)
{
    /* Set stuff up here */
}

void tearDown(void)
{
    /* Clean stuff up here */
}

void test_avg_function(void)
{
    /* Check result */
    TEST_ASSERT_EQUAL_INT_MESSAGE(10, integer_avg(10, 10), "Error in integer_avg");
}

